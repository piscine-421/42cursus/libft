NAME = tester
FLAGS = -Wall -Wextra -Werror -I$(LIBFT) -L$(LIBFT) -lft
LIBFT = Libft
SOURCES = test.c test_escape.c test_is.c test_isalpha.c test_isdigit.c test_isalnum.c test_isascii.c test_isprint.c test_strlen.c test_memset.c test_bzero.c test_memcpy.c test_memmove.c test_strlcpy.c test_strlcat.c test_toupper.c test_tolower.c test_strchr.c test_strrchr.c test_strncmp.c test_memchr.c test_memcmp.c test_strnstr.c test_atoi.c test_calloc.c test_strdup.c test_substr.c test_strjoin.c test_strtrim.c test_split.c test_itoa.c test_strmapi.c test_striteri.c test_putchar_fd.c test_putstr_fd.c test_putendl_fd.c test_putnbr_fd.c test_lstnew.c test_lstadd_front.c test_lstsize.c test_lstlast.c test_lstadd_back.c

all: $(addprefix test/,$(SOURCES))
	@make bonus -s -C $(LIBFT)
	@cc $(FLAGS) $(addprefix test/,$(SOURCES)) -o $(NAME)

clean:
	@make clean -s -C $(LIBFT)

fclean: clean
	@make fclean -s -C $(LIBFT)
	@rm -f $(NAME)

re: fclean all
