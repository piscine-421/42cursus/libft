/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strncmp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:40 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_strncmp(const char *s1, const char *s2, size_t n, t_test *t)
{
	if (!s1 || !s2)
	{
		ft_strncmp(s1, s2, n);
		if (t->f)
			printf("\033[1;32mSuccess on s1 = %s, s2 = %s, n = %lu\n", sesc(s1), sesc(s2), n);
	}
	else if (ft_strncmp(s1, s2, n) != strncmp(s1, s2, n))
	{
		printf("\033[1;31mFailure on s1 = %s, s2 = %s, n = %lu\n", sesc(s1), sesc(s2), n);
		printf("ft_strncmp:	%d\n", ft_strncmp(s1, s2, n));
		printf("strncmp:	%d\n", strncmp(s1, s2, n));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on s1 = %s, s2 = %s, n = %lu\n", sesc(s1), sesc(s2), n);
		printf("ft_strncmp:	%d\n", ft_strncmp(s1, s2, n));
		printf("strncmp:	%d\n", strncmp(s1, s2, n));
	}
}

void	test_strncmp(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strncmp", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strncmp\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_strncmp(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_strncmp("hello world", "hello warld", 7, t);
			check_strncmp("hello world", "hello warld", 8, t);
			check_strncmp("hello world", "hello warld", 20, t);
			check_strncmp("hello world", "hello warld", 0, t);
			check_strncmp("abcdefg", "1234567", 0, t);
			check_strncmp("", "", 0, t);
			check_strncmp("!A@Q!@", "312123", 3, t);
			check_strncmp(0, "312123", 3, t);
			check_strncmp("!A@Q!@", "312123", 3, t);
			check_strncmp("!A@Q!@", 0, 3, t);
			check_strncmp("!A@Q!@", "312123", 0, t);
			check_strncmp(0, 0, 3, t);
			check_strncmp(0, "312123", 0, t);
			check_strncmp("!A@Q!@", 0, 0, t);
			check_strncmp(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_memchr(t, argv);
}
