/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_lstlast.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:42 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_lstlast(t_list *lst, char *expected)
{
	if (!expected)
	{
		ft_lstlast(lst);
		if (g_verbose)
			printf("\033[1;32mSuccess\n");
	}
	else if (strcmp(ft_lstlast(lst)->content, expected))
	{
		printf("\033[1;31mFailure\n");
		printf("ft_lstlast->content:	%s\n", sesc(ft_lstlast(lst)->content));
		printf("expected:		%s\n", sesc(expected));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess\n");
		printf("ft_lstlast->content:	%s\n", sesc(ft_lstlast(lst)->content));
		printf("expected:		%s\n", sesc(expected));
	}
}
