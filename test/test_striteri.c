/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_striteri.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:33 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	changes(unsigned int n, char *s)
{
	s[0] += n;
}

void	shakys(unsigned int n, char *s)
{
	s[0] = tolower(s[0]);
	if (n % 2 == 1 && isalpha(s[0]))
		s[0] = toupper(s[0]);
}

void	test_striteri(char *s, void (*f)(unsigned int, char*), char *expected, char *fname)
{
	char	*s2;

	if (!strcmp(expected, "ARGMODE"))
	{
		s2 = strdup(s);
		ft_striteri(s, f);
		printf("\033[1;33mWARNING: Cannot validate manually selected arguments.\n");
		printf("s = %s, f = %s\n", sesc(s2), fname);
		printf("ft_striteri:	%s\n", sesc(s));
		g_failure = 2;
		free(s2);
	}
	else if (!strcmp(expected, "NULL"))
	{
		ft_striteri(s, f);
		if (g_verbose)
			printf("\033[1;32mSuccess on s = %s, f = %s\n", sesc(s), fname);
	}
	else
	{
		s2 = strdup(s);
		ft_striteri(s, f);
		if (strcmp(expected, s))
		{
			printf("\033[1;31mFailure on s = %s, f = %s\n", sesc(s2), fname);
			printf("ft_striteri:	%s\n", sesc(s));
			printf("expected:	%s\n", sesc(expected));
			g_failure = 1;
		}
		else if (g_verbose)
		{
			printf("\033[1;32mSuccess on s = %s, f = %s\n", sesc(s2), fname);
			printf("ft_striteri:	%s\n", sesc(s));
			printf("expected:	%s\n", sesc(expected));
		}
		free(s2);
	}
}
