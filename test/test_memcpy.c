/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memcpy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:46 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_memcpy(void *dst, const void *src, size_t n, t_test *t)
{
	char	*s[4];

	if (!dst || !src)
	{
		ft_memcpy(dst, src, n);
		if (t->v)
			printf("\033[1;32mSuccess on dst = %s, src = %s, n = %lu.\n", sesc(dst), sesc(src), n);
	}
	else
	{
		s[0] = strdup(dst);
		ft_memcpy(s[0], src, n);
		s[1] = strdup(dst);
		memcpy(s[1], src, n);
		if (strcmp(s[0], s[1]))
		{
			printf("\033[1;31mFailure on dst = %s, src = %s, n = %lu.\n", sesc(dst), sesc(src), n);
			printf("ft_memcpy:	%s\n", sesc(s[0]));
			printf("memcpy:		%s\n", sesc(s[1]));
			t->f = 1;
		}
		else if (t->v)
		{
			printf("\033[1;32mSuccess on dst = %s, src = %s, n = %lu.\n", sesc(dst), sesc(src), n);
			printf("ft_memcpy:	%s\n", sesc(s[0]));
			printf("memcpy:		%s\n", sesc(s[1]));
		}
	}
}

void test_memcpy(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_memcpy", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_memcpy\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_memcpy(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_memcpy("hello world", "halla warld", 11, t);
			check_memcpy("bye wor", "baa warld", 11., t);
			check_memcpy("", "", 0, t);
			check_memcpy("ok", "no", 0, t);
			check_memcpy("ok", "", 3, t);
			check_memcpy("ok", "no", 3, t);
			check_memcpy("afhafdh", "sdgdagaw", 32, t);
			check_memcpy(0, "sdgdagaw", 32, t);
			check_memcpy("afhafdh", 0, 32, t);
			check_memcpy("afhafdh", "sdgdagaw", 0, t);
			check_memcpy(0, 0, 32, t);
			check_memcpy(0, "sdgdagaw", 0, t);
			check_memcpy("afhafdh", 0, 0, t);
			check_memcpy(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_memmove(t, argv);
}