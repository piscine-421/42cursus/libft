/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_lstnew.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:43 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_lstnew(void *content)
{
	t_list	*l;

	l = ft_lstnew(content);
	if (content && strcmp(l->content, content))
	{
		printf("\033[1;31mFailure on content = %s\n", sesc(content));
		printf("*ft_lstnew->content:	%s\n", sesc(l->content));
		printf("content:		%s\n", sesc(content));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on content = %s\n", sesc(content));
		printf("*ft_lstnew->content:	%s\n", sesc(l->content));
		printf("content:		%s\n", sesc(content));
	}
	free(l);
}
