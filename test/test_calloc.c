/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_atoi.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:30 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_calloc(size_t count, size_t size, t_test *t)
{
	int		i;
	char	*str;
	char	*str2;

	i = count * size;
	str = ft_calloc(count, size);
	str2 = calloc(count, size);
	while (i && str[i] == str2[i])
		i--;
	if (i)
	{
		printf("\033[1;31mFailure on count = %lu, size = %lu\n", count, size);
		t->f = 1;
	}
	else if (t->v)
		printf("\033[1;32mSuccess on count = %lu, size = %lu\n", count, size);
	free(str);
	free(str2);
}

void	test_calloc(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_calloc", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_calloc\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]))
		{
			t->v = 1;
			check_calloc(atoi(argv[t->p + 2]), atoi(argv[t->p + 3]), t);
		}
		else
		{
			check_calloc(0, 0, t);
			check_calloc(4, 5, t);
			check_calloc(10, 10, t);
			check_calloc(100, 1, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
}
