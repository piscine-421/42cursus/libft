/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isprint.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:37 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_isprint(int c, t_test *t)
{
	if ((ft_isprint(c) && !isprint(c)) || (!ft_isprint(c) && isprint(c)))
	{
		printf("\033[1;31mFailure on c = %s.\n", cesc(c));
		printf("ft_isprint:	%d\n", ft_isprint(c));
		printf("isprint:	%d\n", isprint(c));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on c = %s.\n", cesc(c));
		printf("ft_isprint:	%d\n", ft_isprint(c));
		printf("isprint:	%d\n", isprint(c));
	}
}

void	test_isprint(t_test *t, char **argv)
{
	int	i;

	if (!t->p || !strcmp("ft_isprint", argv[t->p + 1]))
	{
		i = -1;
		t->f = 0;
		printf("\n\033[39;1;4mft_isprint\033[0m\n");
		if (t->p && i_c(argv[t->p + 2]))
		{
			t->v = 1;
			check_isprint(cunesc(argv[t->p + 2]), t);
		}
		else
			while (i < 256)
				check_isprint(i++, t);
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_strlen(t, argv);
}
