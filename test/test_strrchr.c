/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strrchr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:42 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	check_strrchr(const char *s, int c, t_test *t)
{
	if (!s)
	{
		ft_strrchr(s, c);
		if (t->v)
			printf("\033[1;32mSuccess on s = %s, c = %s\n", sesc(s), cesc(c));
	}
	else if (strrchr(s, c) != ft_strrchr(s, c))
	{
		printf("\033[1;31mFailure on s = %s, c = %s\n", sesc(s), cesc(c));
		printf("ft_strrchr:	%s\n", sesc(ft_strrchr(s, c)));
		printf("strrchr:	%s\n", sesc(strrchr(s, c)));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on s = %s, c = %s\n", sesc(s), cesc(c));
		printf("ft_strrchr:	%s\n", sesc(ft_strrchr(s, c)));
		printf("strrchr:	%s\n", sesc(strrchr(s, c)));
	}
}

void	test_strrchr(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strrchr", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strrchr\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_c(argv[t->p + 3]))
		{
			t->v = 1;
			check_strrchr(sunesc(argv[t->p + 2]), cunesc(argv[t->p + 3]), t);
		}
		else
		{
			check_strrchr("ALLO TOUT LE MONDE", 'T', t);
			check_strrchr("SDGSGRWRDG", 'a', t);
			check_strrchr("", 'a', t);
			check_strrchr("SRGSQW", '\0', t);
			check_strrchr("asdgaryertje75srhdrwerq3vtws", 't' + 256, t);
			check_strrchr(0, 't' + 256, t);
			check_strrchr("asdgaryertje75srhdrwerq3vtws", 0, t);
			check_strrchr(0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_strncmp(t, argv);
}
