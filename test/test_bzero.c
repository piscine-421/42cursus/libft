/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_bzero.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:31 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_bzero(void *s, size_t n, t_test *t)
{
	char	*s1;
	char	*s2;

	if (!s)
	{
		ft_bzero(s, n);
		if (t->v)
			printf("\033[1;32mSuccess on s = %s, n = %lu.\n", sesc(s), n);
	}
	else
	{
		s1 = strdup(s);
		ft_bzero(s1, n);
		s2 = strdup(s);
		bzero(s2, n);
		if (strcmp(s1, s2))
		{
			printf("\033[1;31mFailure on s = %s, n = %lu.\n", sesc(s), n);
			printf("ft_bzero:	%s\n", sesc(s1));
			printf("bzero:		%s\n", sesc(s2));
			t->f = 1;
		}
		else if (t->v)
		{
			printf("\033[1;32mSuccess on s = %s, n = %lu.\n", sesc(s), n);
			printf("ft_bzero:	%s\n", sesc(s1 + n));
			printf("bzero:		%s\n", sesc(s2 + n));
		}
	}
}

void	test_bzero(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_bzero", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_bzero\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_d(argv[t->p + 3]))
		{
			t->v = 1;
			check_bzero(sunesc(argv[t->p + 2]), atoi(argv[t->p + 3]), t);
		}
		else
		{
			check_bzero("hello world", 5, t);
			check_bzero("YO GUYS ITS ME OK BYE", 0, t);
			check_bzero("allo", 89, t);
			check_bzero("", 3, t);
			check_bzero(0, 3, t);
			check_bzero("", 0, t);
			check_bzero(0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s ==t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
	t->t++;
	}
	test_memcpy(t, argv);
}
