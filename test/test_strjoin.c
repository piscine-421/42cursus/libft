/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strjoin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:35 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_strjoin(char const *s1, char const *s2, char *expected)
{
	if (!strcmp(expected, "ARGMODE"))
	{
		printf("\033[1;33mWARNING: Cannot validate manually selected arguments.\n");
		printf("s1 = %s, s2 = %s\n", sesc(s1), sesc(s2));
		printf("ft_strjoin:	%s\n", sesc(ft_strjoin(s1, s2)));
		g_failure = 2;
	}
	else if (!strcmp(expected, "NULL"))
	{
		ft_strjoin(s1, s2);
		if (g_verbose)
			printf("\033[1;32mSuccess on s1 = \"%s\", s2 = \"%s\"\n", s1, s2);
	}
	else if (strcmp(ft_strjoin(s1, s2), expected))
	{
		printf("\033[1;31mFailure on %s, s2 = %s\n", sesc(s1), sesc(s2));
		printf("ft_strjoin:	%s\n", sesc(ft_strjoin(s1, s2)));
		printf("expected:	%s\n", sesc(expected));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on %s, s2 = %s\n", sesc(s1), sesc(s2));
		printf("ft_strjoin:	%s\n", sesc(ft_strjoin(s1, s2)));
		printf("expected:	%s\n", sesc(expected));
	}
}
