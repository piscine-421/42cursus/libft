/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_atoi.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:30 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_atoi(const char *str, t_test *t)
{
	if (!str)
	{
		ft_atoi(str);
		if (t->v)
			printf("\033[1;32mSuccess on str = %s\n", sesc(str));
	}
	else if (atoi(str) != ft_atoi(str))
	{
		printf("\033[1;31mFailure on str = %s\n", sesc(str));
		printf("ft_atoi:	%d\n", ft_atoi(str));
		printf("atoi:		%d\n", atoi(str));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on str = %s\n", sesc(str));
		printf("ft_atoi:	%d\n", ft_atoi(str));
		printf("atoi:		%d\n", atoi(str));
	}
}

void	test_atoi(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_atoi", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_atoi\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]))
		{
			t->v = 1;
			check_atoi(sunesc(argv[t->p + 2]), t);
		}
		else
		{
			check_atoi("	\t	\n	-32133235", t);
			check_atoi("	\t	+23\n	-32133235", t);
			check_atoi("", t);
			check_atoi("	\t	\n	-2147483648", t);
			check_atoi("	\t	\n	-", t);
			check_atoi("	\t	\n	-+2323", t);
			check_atoi("	\t	\n	- 2323", t);
			check_atoi("1 \v	\n	- 2323", t);
			check_atoi(0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_calloc(t, argv);
}
