/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strnstr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:41 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_strnstr(char *haystack, const char *needle, size_t len, t_test *t)
{
	if (!haystack || !needle)
	{
		ft_strnstr(haystack, needle, len);
		if (t->v)
			printf("\033[1;32mSuccess on haystack = %s, needle = %s, len = %lu\n", sesc(haystack), sesc(needle), len);
	}
	else if (strnstr(haystack, needle, len) != ft_strnstr(haystack, needle, len))
	{
		printf("\033[1;31mFailure on haystack = %s, needle = %s, len = %lu\n", sesc(haystack), sesc(needle), len);
		printf("ft_strnstr:	%s\n", sesc(ft_strnstr(haystack, needle, len)));
		printf("strnstr:	%s\n", sesc(strnstr(haystack, needle, len)));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on haystack = %s, needle = %s, len = %lu\n", sesc(haystack), sesc(needle), len);
		printf("ft_strnstr:	%s\n", sesc(ft_strnstr(haystack, needle, len)));
		printf("strnstr:	%s\n", sesc(strnstr(haystack, needle, len)));
	}
}

void	test_strnstr(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strnstr", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strnstr\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_strnstr(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_strnstr("ALLO TOUT LE MONDE", "TOUT", 19, t);
			check_strnstr("ALLO TOUT LE MONDE", "TOU T", 19, t);
			check_strnstr("ALLO TOUT LE MONDE", "TOUT", 8, t);
			check_strnstr("", "", 1, t);
			check_strnstr("", "", 0, t);
			check_strnstr("TOUT TOUT TOUT TOUT TOUT", "TOUT", 90, t);
			check_strnstr(0, "TOUT", 90, t);
			check_strnstr("TOUT TOUT TOUT TOUT TOUT", 0, 90, t);
			check_strnstr("TOUT TOUT TOUT TOUT TOUT", "TOUT", 0, t);
			check_strnstr(0, 0, 90, t);
			check_strnstr(0, "TOUT", 0, t);
			check_strnstr("TOUT TOUT TOUT TOUT TOUT", 0, 0, t);
			check_strnstr(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_atoi(t, argv);
}
