/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_lstadd_front.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:39 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_lstadd_front(char *lstc, char *newc)
{
	t_list	**lst;
	t_list	*new;

	lst = malloc(sizeof(t_list) * 2);
	*lst = ft_lstnew(lstc);
	new = ft_lstnew(newc);
	ft_lstadd_front(lst, new);
	if (lstc && newc && (strcmp(lst[0]->content, new->content) || strcmp(lst[0]->next->content, lstc)))
	{
		printf("\033[1;31mFailure on *lst->content = %s, new->content = %s\n", sesc(lstc), sesc(newc));
		printf("ft_lstadd_front[0]->content:		%s\n", sesc(lst[0]->content));
		printf("ft_lstadd_front[0]->next->content:	%s\n", sesc(lst[0]->next->content));
		printf("lst[0]->content:			%s\n", sesc(new->content));
		printf("new->content:				%s\n", sesc(lstc));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on *lst->content = %s, new->content = %s\n", sesc(lstc), sesc(newc));
		printf("ft_lstadd_front[0]->content:		%s\n", sesc(lst[0]->content));
		printf("ft_lstadd_front[0]->next->content:	%s\n", sesc(lst[0]->next->content));
		printf("lst[0]->content:			%s\n", sesc(new->content));
		printf("new->content:				%s\n", sesc(lstc));
	}
	free(lst);
	free(new);
}
