/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memset.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:48 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_memset(void *b, int c, size_t len, t_test *t)
{
	char	*s[2];

	if (b)
		s[0] = strdup(b);
	ft_memset(s[0], c, len);
	if (b)
		s[1] = strdup(b);
	memset(s[1], c, len);
	if (strncmp(s[0], s[1], len))
	{
		printf("\033[1;31mFailure on b = %s, c = %s, len = %lu.\n", sesc(b), cesc(c), len);
		printf("ft_memset:	%s\n", sesc(s[0]));
		printf("memset:		%s\n", sesc(s[1]));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on b = %s, c = %s, len = %lu.\n", sesc(b), cesc(c), len);
		printf("ft_memset:	%s\n", sesc(s[0]));
		printf("memset:		%s\n", sesc(s[1]));
	}
}

void	test_memset(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_memset", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_memset\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_c(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_memset(sunesc(argv[t->p + 2]), cunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_memset("hello world", 'A', 5, t);
			check_memset("YO GUYS ITS ME OK BYE", ' ', 0, t);
			check_memset("allo", '0', 89, t);
			check_memset("", 'w', 3, t);
			check_memset(0, 'w', 3, t);
			check_memset("", 0, 3, t);
			check_memset("", 'w', 0, t);
			check_memset(0, 0, 3, t);
			check_memset(0, 'w', 0, t);
			check_memset("", 0, 0, t);
			check_memset(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_bzero(t, argv);
}
