/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isdigit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:36 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_isdigit(int c, t_test *t)
{
	if ((ft_isdigit(c) && !isdigit(c)) || (!ft_isdigit(c) && isdigit(c)))
	{
		printf("\033[1;31mFailure on c = %s.\n", cesc(c));
		printf("ft_isdigit:	%d\n", ft_isdigit(c));
		printf("isdigit:	%d\n", isdigit(c));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on c = %s.\n", cesc(c));
		printf("ft_isdigit:	%d\n", ft_isdigit(c));
		printf("isdigit:	%d\n", isdigit(c));
	}
}

void	test_isdigit(t_test *t, char **argv)
{
	int	i;

	if (!t->p || !strcmp("ft_isdigit", argv[t->p + 1]))
	{
		i = -1;
		t->f = 0;
		printf("\n\033[39;1;4mft_isdigit\033[0m\n");
		if (t->p && i_c(argv[t->p + 2]))
		{
			t->v = 1;
			check_isdigit(cunesc(argv[t->p + 2]), t);
		}
		else
			while (i < 256)
				check_isdigit(i++, t);
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_isalnum(t, argv);
}
