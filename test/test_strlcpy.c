/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlcpy.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:37 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_strlcpy(char *dst, const char *src, size_t dstsize, t_test *t)
{
	size_t	r[2];
	char	*s[2];

	if (!dst || !src)
	{
		ft_strlcpy(dst, src, dstsize);
		if (t->v)
			printf("\033[1;32mSuccess on dst = %s, src = %s, dstsize = %lu.\n", sesc(dst), sesc(src), dstsize);
	}
	else
	{
		s[0] = strdup(dst);
		r[0] = ft_strlcpy(s[0], src, dstsize);
		s[1] = strdup(dst);
		r[1] = strlcpy(s[1], src, dstsize);
		if (strcmp(s[0], s[1]) || r[0] != r[1])
		{
			printf("\033[1;31mFailure on dst = %s, src = %s, dstsize = %lu.\n", sesc(dst), sesc(src), dstsize);
			printf("ft_strlcpy:	%s (%lu)\n", sesc(s[0]), r[0]);
			printf("strlcpy:	%s (%lu)\n", sesc(s[1]), r[1]);
			t->f = 1;
		}
		else if (t->v)
		{
			printf("\033[1;32mSuccess on dst = %s, src = %s, dstsize = %lu.\n", sesc(dst), sesc(src), dstsize);
			printf("ft_strlcpy:	%s (%lu)\n", sesc(s[0]), r[0]);
			printf("strlcpy:	%s (%lu)\n", sesc(s[1]), r[1]);
		}
		free(*s);
	}
}

void	test_strlcpy(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strlcpy", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strlcpy\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_strlcpy(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_strlcpy("hello world", "halla warld", 11, t);
			check_strlcpy("bye wor", "baa warld", 11, t);
			check_strlcpy("", "", 0, t);
			check_strlcpy("", "", 3, t);
			check_strlcpy("ok", "no", 0, t);
			check_strlcpy("ok", "", 3, t);
			check_strlcpy("ok", "no", 3, t);
			check_strlcpy("hahahahaha", "hohohohoho", 6, t);
			check_strlcpy("hahahahaha", "hohohohoho", 20, t);
			check_strlcpy(0, "hohohohoho", 20, t);
			check_strlcpy("hahahahaha", 0, 20, t);
			check_strlcpy("hahahahaha", "hohohohoho", 0, t);
			check_strlcpy(0, 0, 20, t);
			check_strlcpy(0, "hohohohoho", 0, t);
			check_strlcpy("hahahahaha", 0, 0, t);
			check_strlcpy(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_strlcat(t, argv);
}
