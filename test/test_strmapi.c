/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strmapi.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:39 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

char	changec(unsigned int n, char c)
{
	return (c + n);
}

char	shakyc(unsigned int n, char c)
{
	c = tolower(c);
	if (n % 2 == 1 && isalpha(c))
		c = toupper(c);
	return (c);
}

void	test_strmapi(char const *s, char (*f)(unsigned int, char), char *expected, char *fname)
{
	char	*s2;

	s2 = ft_strmapi(s, f);
	if (!strcmp(expected, "ARGMODE"))
	{
		printf("\033[1;33mWARNING: Cannot validate manually selected arguments.\n");
		printf("s = %s, f = %s\n", sesc(s), fname);
		printf("ft_strmapi:	%s\n", sesc(s2));
		g_failure = 2;
	}
	else if (!strcmp(expected, "NULL"))
	{
		ft_strmapi(s, f);
		if (g_verbose)
			printf("\033[1;32mSuccess on s = %s, f = %s\n", sesc(s), fname);
	}
	else if (strcmp(s2, expected))
	{
		printf("\033[1;31mFailure on s = %s, f = %s\n", sesc(s), fname);
		printf("ft_strmapi:	%s\n", sesc(s2));
		printf("expected:	%s\n", sesc(expected));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on s = %s, f = %s\n", sesc(s), fname);
		printf("ft_strmapi:	%s\n", sesc(s2));
		printf("expected:	%s\n", sesc(expected));
	}
	free(s2);
}
