/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_memchr(const void *s, int c, size_t n, t_test *t)
{
	if (!s)
	{
		ft_memchr(s, c, n);
		if (t->v)
			printf("\033[1;32mSuccess on s = %s, c = %s, n = %lu\n", sesc(s), cesc(c), n);
	}
	else if (memchr(s, c, n) != ft_memchr(s, c, n))
	{
		printf("\033[1;31mFailure on s = %s, c = %s, n = %lu\n", sesc(s), cesc(c), n);
		printf("ft_memchr:	%s\n", sesc(ft_memchr(s, c, n)));
		printf("memchr:		%s\n", sesc(memchr(s, c, n)));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on s = %s, c = %s, n = %lu\n", sesc(s), cesc(c), n);
		printf("ft_memchr:	%s\n", sesc(ft_memchr(s, c, n)));
		printf("memchr:		%s\n", sesc(memchr(s, c, n)));
	}
}

void	test_memchr(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_memchr", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_memchr\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_c(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_memchr(sunesc(argv[t->p + 2]), cunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_memchr("ALLO TOUT LE MONDE", 'T', 19, t);
			check_memchr("SDGSGRWRDG", 'a', 0, t);
			check_memchr("", 'a', 6, t);
			check_memchr("SRGSQW", '\0', 3, t);
			check_memchr("bonjourno", 'n', 2, t);
			check_memchr("test1", '\0', 0, t);
			check_memchr("tesat3", 'a' + 256, 4, t);
			check_memchr(0, 'a' + 256, 4, t);
			check_memchr("tesat3", 0, 4, t);
			check_memchr("tesat3", 'a' + 256, 0, t);
			check_memchr(0, 0, 4, t);
			check_memchr(0, 'a' + 256, 0, t);
			check_memchr("tesat3", 0, 0, t);
			check_memchr(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_memcmp(t, argv);
}
