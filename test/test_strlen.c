/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:38 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_strlen(const char *s, t_test *t)
{
	if (!s)
	{
		ft_strlen(s);
		if (t->v)
		{
			printf("\033[1;32mSuccess on s = %s.\n", sesc(s));
			printf("ft_strlen:	%lu\n", ft_strlen(s));
		}
	}
	else if (ft_strlen(s) != strlen(s))
	{
		printf("\033[1;31mFailure on s = %s.\n", sesc(s));
		printf("ft_strlen:	%lu\n", ft_strlen(s));
		printf("strlen:		%lu\n", strlen(s));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on s = %s.\n", sesc(s));
		printf("ft_strlen:	%lu\n", ft_strlen(s));
		printf("strlen:		%lu\n", strlen(s));
	}
}

void	test_strlen(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strlen", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strlen\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]))
		{
			t->v = 1;
			check_strlen(sunesc(argv[t->p + 2]), t);
		}
		else
		{
			check_strlen("hello world", t);
			check_strlen("YO GUYS ITS ME OK BYE", t);
			check_strlen("allo", t);
			check_strlen("", t);
			check_strlen(0, t);
		}
		if (!g_failure)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_memset(t, argv);
}
