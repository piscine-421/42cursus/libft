/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strtrim.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:43 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_strtrim(char const *s1, char const *set, char *expected)
{
	if (!strcmp(expected, "ARGMODE"))
	{
		printf("\033[1;33mWARNING: Cannot validate manually selected arguments.\n");
		printf("s1 = %s, set = %s\n", sesc(s1), sesc(set));
		printf("ft_strtrim:	%s\n", sesc(ft_strtrim(s1, set)));
		g_failure = 2;
	}
	else if (!strcmp(expected, "NULL"))
	{
		ft_strtrim(s1, set);
		if (g_verbose)
			printf("\033[1;32mSuccess on s1 = %s, set = %s\n", sesc(s1), sesc(set));
	}
	else if (strcmp(ft_strtrim(s1, set), expected))
	{
		printf("\033[1;31mFailure on s1 = %s, set = %s\n", sesc(s1), sesc(set));
		printf("ft_strtrim:	%s\n", sesc(ft_strtrim(s1, set)));
		printf("expected:	%s\n", sesc(expected));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on s1 = %s, set = %s\n", sesc(s1), sesc(set));
		printf("ft_strtrim:	%s\n", sesc(ft_strtrim(s1, set)));
		printf("expected:	%s\n", sesc(expected));
	}
}
