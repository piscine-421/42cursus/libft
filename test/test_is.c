/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_is.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:33 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

int	i_c(char	*s)
{
	return (s && (strlen(s) == 1 || (strlen(s) < 5 && s[0] == '\\')));
}

int	i_d(char *s)
{
	int	i;

	i = 0;
	if (!s)
		return (0);
	if (s[i] == '-')
		i++;
	while (s[i])
	{
		if (!isdigit(s[i]))
			return (0);
		i++;
	}
	return (1);
}

int	i_s(char *s)
{
	return (s && strcmp(s, "-v"));
}
