/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:48 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

int	main(int argc, char **argv)
{
	t_test	t;

	t.f = 0;
	t.p = 0;
	t.s = 0;
	t.t = 0;
	t.v = 0;
	while (argc--)
	{
		if (!strcmp(argv[argc], "-h"))
		{
			printf("Libft Test Program by Léopold Couturier-Otis.\n\n");
			printf("Usage: %s [-hv] [-t function argument(s)]\n\n", argv[0]);
			printf("  h : display this help message\n");
			printf("  v : display more information\n\n");
			printf("  t function : test specified function with specified argument(s)\n");
			printf("               (if arguments missing/invalid: built-in tests used)\n");
			printf("               (if function missing or invalid: argument ignored)\n");
			return (0);
		}
		if (!strcmp(argv[argc], "-v"))
			t.v = 1;
		if (!t.p && !strcmp(argv[argc], "-t") && argv[argc + 1] && strcmp(argv[argc + 1], "-v"))
			t.p = argc;
	}
	test_isalpha(&t, argv);
	// if (!t->p || !strcmp("ft_strdup", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_strdup\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]))
	// 	{
	// 		t->v = 1;
	// 		check_strdup(sunesc(argv[t->p + 2]));
	// 	}
	// 	else
	// 	{
	// 		check_strdup("Rien.");
	// 		check_strdup("");
	// 		check_strdup("IT'S A ME MARIO");
	// 		check_strdup("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	// 		check_strdup("ABCDEFGSHAWERFODPSHJW)E(MNDIOZXNGEPOJKW)");
	// 		check_strdup(0);
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_substr", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_substr\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_d(argv[t->p + 3]) && i_d(argv[t->p + 4]))
	// 	{
	// 		t->v = 1;
	// 		check_substr(sunesc(argv[t->p + 2]), atoi(argv[t->p + 3]), atoi(argv[t->p + 4]), "ARGMODE");
	// 	}
	// 	else
	// 	{
	// 		check_substr("Rien.", 5, 5, "");
	// 		check_substr("", 0, 0, "");
	// 		check_substr("IT'S A ME MARIO", 4, 5, " A ME");
	// 		check_substr("IT'S A ME MARIO", 0, 15, "IT'S A ME MARIO");
	// 		check_substr("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 5, 2, "AA");
	// 		check_substr("ABCDEFGSHAWERFODPSHJW)E(MNDIOZXNGEPOJKW)", 11, 12, "ERFODPSHJW)E");
	// 		check_substr("", 400, 20, "");
	// 		check_substr("", 5, 0, "");
	// 		check_substr("01234", 10, 10, "");
	// 		check_substr("", 1, 1, "");
	// 		check_substr(0, 1, 1, "NULL");
	// 		check_substr("", 0, 1, "");
	// 		check_substr("", 1, 0, "");
	// 		check_substr(0, 0, 1, "NULL");
	// 		check_substr(0, 1, 0, "NULL");
	// 		check_substr("", 0, 0, "");
	// 		check_substr(0, 0, 0, "NULL");
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_strjoin", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_strjoin\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_strjoin(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), "ARGMODE");
	// 	}
	// 	else
	// 	{
	// 		check_strjoin("Rien", "... Ou pas.", "Rien... Ou pas.");
	// 		check_strjoin("", "", "");
	// 		check_strjoin("IT'S A ME", " MARIO", "IT'S A ME MARIO");
	// 		check_strjoin("AAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
	// 		check_strjoin("ASFD", "", "ASFD");
	// 		check_strjoin("", "ASFD", "ASFD");
	// 		check_strjoin("lorem ipsum", "dolor sit amet", "lorem ipsumdolor sit amet");
	// 		check_strjoin("", "dolor sit amet", "dolor sit amet");
	// 		check_strjoin("lorem ipsum", "", "lorem ipsum");
	// 		check_strjoin(0, "", "NULL");
	// 		check_strjoin("lorem ipsum", 0, "NULL");
	// 		check_strjoin(0, 0, "NULL");
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_strtrim", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_strtrim\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_strtrim(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), "ARGMODE");
	// 	}
	// 	else
	// 	{
	// 		check_strtrim("Rien... Ou pas.", "a", "Rien... Ou pas.");
	// 		check_strtrim("", "", "");
	// 		check_strtrim("IT'S A ME", " MARIO", "T'S A ME");
	// 		check_strtrim("AAAAAAAAAAAAAAA", "AAAAAAAAAAAAAAAAAAA", "");
	// 		check_strtrim("ASFD", "AD", "SF");
	// 		check_strtrim("", "ASFD", "");
	// 		check_strtrim(0, "ASFD", "NULL");
	// 		check_strtrim("", 0, "NULL");
	// 		check_strtrim(0, 0, "NULL");
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_split", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_split\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_c(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		expected = malloc(8);
	// 		expected[0] = "ARGMODE";
	// 		check_split(sunesc(argv[t->p + 2]), cunesc(argv[t->p + 3]), expected);
	// 		free(expected);
	// 	}
	// 	else
	// 	{
	// 		expected = malloc(72);
	// 		expected[0] = strdup("k");
	// 		expected[1] = strdup("tefichiereditionselectionner");
	// 		expected[2] = strdup("ffich");
	// 		expected[3] = strdup("ge");
	// 		expected[4] = strdup("llerprojetsconstruirect");
	// 		expected[5] = strdup("gsxmlsessionsoutilsconfigur");
	// 		expected[6] = strdup("tion");
	// 		expected[7] = strdup("ide");
	// 		expected[8] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'a', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[1] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'b', expected);
	// 		free(expected);
	// 		expected = malloc(64);
	// 		expected[0] = strdup("katefi");
	// 		expected[1] = strdup("hiereditionsele");
	// 		expected[2] = strdup("tionneraffi");
	// 		expected[3] = strdup("hageallerprojets");
	// 		expected[4] = strdup("onstruire");
	// 		expected[5] = strdup("tagsxmlsessionsoutils");
	// 		expected[6] = strdup("onfigurationaide");
	// 		expected[7] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'c', expected);
	// 		free(expected);
	// 		expected = malloc(32);
	// 		expected[0] = strdup("katefichiere");
	// 		expected[1] = strdup("itionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationai");
	// 		expected[2] = strdup("e");
	// 		expected[3] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'd', expected);
	// 		free(expected);
	// 		expected = malloc(104);
	// 		expected[0] = strdup("kat");
	// 		expected[1] = strdup("fichi");
	// 		expected[2] = strdup("r");
	// 		expected[3] = strdup("ditions");
	// 		expected[4] = strdup("l");
	// 		expected[5] = strdup("ctionn");
	// 		expected[6] = strdup("raffichag");
	// 		expected[7] = strdup("all");
	// 		expected[8] = strdup("rproj");
	// 		expected[9] = strdup("tsconstruir");
	// 		expected[10] = strdup("ctagsxmls");
	// 		expected[11] = strdup("ssionsoutilsconfigurationaid");
	// 		expected[12] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'e', expected);
	// 		free(expected);
	// 		expected = malloc(40);
	// 		expected[0] = strdup("kate");
	// 		expected[1] = strdup("ichiereditionselectionnera");
	// 		expected[2] = strdup("ichageallerprojetsconstruirectagsxmlsessionsoutilscon");
	// 		expected[3] = strdup("igurationaide");
	// 		expected[4] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'f', expected);
	// 		free(expected);
	// 		expected = malloc(40);
	// 		expected[0] = strdup("katefichiereditionselectionnerafficha");
	// 		expected[1] = strdup("eallerprojetsconstruirecta");
	// 		expected[2] = strdup("sxmlsessionsoutilsconfi");
	// 		expected[3] = strdup("urationaide");
	// 		expected[4] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'g', expected);
	// 		free(expected);
	// 		expected = malloc(32);
	// 		expected[0] = strdup("katefic");
	// 		expected[1] = strdup("iereditionselectionneraffic");
	// 		expected[2] = strdup("ageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[3] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'h', expected);
	// 		free(expected);
	// 		expected = malloc(112);
	// 		expected[0] = strdup("katef");
	// 		expected[1] = strdup("ch");
	// 		expected[2] = strdup("ered");
	// 		expected[3] = strdup("t");
	// 		expected[4] = strdup("onselect");
	// 		expected[5] = strdup("onneraff");
	// 		expected[6] = strdup("chageallerprojetsconstru");
	// 		expected[7] = strdup("rectagsxmlsess");
	// 		expected[8] = strdup("onsout");
	// 		expected[9] = strdup("lsconf");
	// 		expected[10] = strdup("gurat");
	// 		expected[11] = strdup("ona");
	// 		expected[12] = strdup("de");
	// 		expected[13] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'i', expected);
	// 		free(expected);
	// 		expected = malloc(24);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerpro");
	// 		expected[1] = strdup("etsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[2] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'j', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("atefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[1] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'k', expected);
	// 		free(expected);
	// 		expected = malloc(48);
	// 		expected[0] = strdup("katefichiereditionse");
	// 		expected[1] = strdup("ectionneraffichagea");
	// 		expected[2] = strdup("erprojetsconstruirectagsxm");
	// 		expected[3] = strdup("sessionsouti");
	// 		expected[4] = strdup("sconfigurationaide");
	// 		expected[5] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'l', expected);
	// 		free(expected);
	// 		expected = malloc(24);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstruirectagsx");
	// 		expected[1] = strdup("lsessionsoutilsconfigurationaide");
	// 		expected[2] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'm', expected);
	// 		free(expected);
	// 		expected = malloc(64);
	// 		expected[0] = strdup("katefichiereditio");
	// 		expected[1] = strdup("selectio");
	// 		expected[2] = strdup("eraffichageallerprojetsco");
	// 		expected[3] = strdup("struirectagsxmlsessio");
	// 		expected[4] = strdup("soutilsco");
	// 		expected[5] = strdup("figuratio");
	// 		expected[6] = strdup("aide");
	// 		expected[7] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'n', expected);
	// 		free(expected);
	// 		expected = malloc(80);
	// 		expected[0] = strdup("katefichierediti");
	// 		expected[1] = strdup("nselecti");
	// 		expected[2] = strdup("nneraffichageallerpr");
	// 		expected[3] = strdup("jetsc");
	// 		expected[4] = strdup("nstruirectagsxmlsessi");
	// 		expected[5] = strdup("ns");
	// 		expected[6] = strdup("utilsc");
	// 		expected[7] = strdup("nfigurati");
	// 		expected[8] = strdup("naide");
	// 		expected[9] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'o', expected);
	// 		free(expected);
	// 		expected = malloc(24);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichagealler");
	// 		expected[1] = strdup("rojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[2] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'p', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[1] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'q', expected);
	// 		free(expected);
	// 		expected = malloc(72);
	// 		expected[0] = strdup("katefichie");
	// 		expected[1] = strdup("editionselectionne");
	// 		expected[2] = strdup("affichagealle");
	// 		expected[3] = strdup("p");
	// 		expected[4] = strdup("ojetsconst");
	// 		expected[5] = strdup("ui");
	// 		expected[6] = strdup("ectagsxmlsessionsoutilsconfigu");
	// 		expected[7] = strdup("ationaide");
	// 		expected[8] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'r', expected);
	// 		free(expected);
	// 		expected = malloc(80);
	// 		expected[0] = strdup("katefichieredition");
	// 		expected[1] = strdup("electionneraffichageallerprojet");
	// 		expected[2] = strdup("con");
	// 		expected[3] = strdup("truirectag");
	// 		expected[4] = strdup("xml");
	// 		expected[5] = strdup("e");
	// 		expected[6] = strdup("ion");
	// 		expected[7] = strdup("outil");
	// 		expected[8] = strdup("configurationaide");
	// 		expected[9] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 's', expected);
	// 		free(expected);
	// 		expected = malloc(80);
	// 		expected[0] = strdup("ka");
	// 		expected[1] = strdup("efichieredi");
	// 		expected[2] = strdup("ionselec");
	// 		expected[3] = strdup("ionneraffichageallerproje");
	// 		expected[4] = strdup("scons");
	// 		expected[5] = strdup("ruirec");
	// 		expected[6] = strdup("agsxmlsessionsou");
	// 		expected[7] = strdup("ilsconfigura");
	// 		expected[8] = strdup("ionaide");
	// 		expected[9] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 't', expected);
	// 		free(expected);
	// 		expected = malloc(40);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstr");
	// 		expected[1] = strdup("irectagsxmlsessionso");
	// 		expected[2] = strdup("tilsconfig");
	// 		expected[3] = strdup("rationaide");
	// 		expected[4] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'u', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[1] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'v', expected);
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'w', expected);
	// 		free(expected);
	// 		expected = malloc(24);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstruirectags");
	// 		expected[1] = strdup("mlsessionsoutilsconfigurationaide");
	// 		expected[2] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'x', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide");
	// 		expected[1] = 0;
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'y', expected);
	// 		check_split("katefichiereditionselectionneraffichageallerprojetsconstruirectagsxmlsessionsoutilsconfigurationaide", 'z', expected);
	// 		free(expected);
	// 		expected = malloc(104);
	// 		expected[0] = strdup("lorem");
	// 		expected[1] = strdup("ipsum");
	// 		expected[2] = strdup("dolor");
	// 		expected[3] = strdup("sit");
	// 		expected[4] = strdup("amet,");
	// 		expected[5] = strdup("consectetur");
	// 		expected[6] = strdup("adipiscing");
	// 		expected[7] = strdup("elit.");
	// 		expected[8] = strdup("Sed");
	// 		expected[9] = strdup("non");
	// 		expected[10] = strdup("risus.");
	// 		expected[11] = strdup("Suspendisse");
	// 		expected[12] = 0;
	// 		check_split("lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse", ' ', expected);
	// 		check_split("   lorem   ipsum dolor     sit amet, consectetur   adipiscing elit. Sed non risus. Suspendisse   ", ' ', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("hello!");
	// 		expected[1] = 0;
	// 		check_split("xxxxxxxxhello!", 'x', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("hello!");
	// 		expected[1] = 0;
	// 		check_split("hello!", ' ', expected);
	// 		free(expected);
	// 		expected = malloc(8);
	// 		expected[0] = strdup("NULL");
	// 		check_split(0, ' ', expected);
	// 		free(expected);
	// 		expected = malloc(16);
	// 		expected[0] = strdup("hello!");
	// 		expected[1] = 0;
	// 		check_split("hello!", 0, expected);
	// 		free(expected);
	// 		expected = malloc(8);
	// 		expected[0] = strdup("NULL");
	// 		check_split(0, 0, expected);
	// 		free(expected);
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_itoa", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_itoa\033[0m\n");
	// 	if (t->p && i_d(argv[t->p + 2]))
	// 	{
	// 		t->v = 1;
	// 		check_itoa(argv[t->p + 2]);
	// 	}
	// 	else
	// 	{
	// 		check_itoa("4567");
	// 		check_itoa("-1235235");
	// 		check_itoa("-2147483648");
	// 		check_itoa("2147483647");
	// 		check_itoa("0");
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_strmapi", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_strmapi\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		if (!strcmp(argv[t->p + 3], "changec"))
	// 			check_strmapi(sunesc(argv[t->p + 2]), changec, "ARGMODE", "changec");
	// 		else if (!strcmp(argv[t->p + 3], "shakyc"))
	// 			check_strmapi(sunesc(argv[t->p + 2]), shakyc, "ARGMODE", "shakyc");
	// 		else
	// 			t->f = 0;
	// 	}
	// 	if (!t->f)
	// 	{
	// 		check_strmapi("0000000000", changec, "0123456789", "changec");
	// 		check_strmapi("9876543210", changec, "9999999999", "changec");
	// 		check_strmapi("aaAFWEFdsf", shakyc, "aAaFwEfDsF", "shakyc");
	// 		check_strmapi("abcdefghijklmnopqrstuvwxyz", shakyc, "aBcDeFgHiJkLmNoPqRsTuVwXyZ", "shakyc");
	// 		check_strmapi(0, shakyc, "NULL", "shakyc");
	// 		check_strmapi("abcdefghijklmnopqrstuvwxyz", 0, "NULL", "(null)");
	// 		check_strmapi(0, 0, "NULL", "(null)");
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_striteri", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_striteri\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		if (!strcmp(argv[t->p + 3], "changes"))
	// 			check_striteri(sunesc(argv[t->p + 2]), changes, "ARGMODE", "changes");
	// 		else if (!strcmp(argv[t->p + 3], "shakys"))
	// 			check_striteri(sunesc(argv[t->p + 2]), shakys, "ARGMODE", "shakys");
	// 		else
	// 			t->f = 0;
	// 	}
	// 	if (!t->f)
	// 	{
	// 		check_striteri(strdup("0000000000"), changes, "0123456789", "changes");
	// 		check_striteri(strdup("9876543210"), changes, "9999999999", "changes");
	// 		check_striteri(strdup("aaAFWEFdsf"), shakys, "aAaFwEfDsF", "shakys");
	// 		check_striteri(strdup("abcdefghijklmnopqrstuvwxyz"), shakys, "aBcDeFgHiJkLmNoPqRsTuVwXyZ", "shakys");
	// 		check_striteri(0, shakys, "NULL", "shakys");
	// 		check_striteri(strdup("abcdefghijklmnopqrstuvwxyz"), 0, "NULL", "(null)");
	// 		check_striteri(0, 0, "NULL", "(null)");
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_putchar_fd", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_putchar_fd\033[0m\n");
	// 	printf("\033[1;33mWARNING: Cannot validate this function.\n");
	// 	if (t->p && i_c(argv[t->p + 2]) && i_d(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_putchar_fd(cunesc(argv[t->p + 2]), atoi(argv[t->p + 3]));
	// 	}
	// 	else if (t->v)
	// 	{
	// 		check_putchar_fd('z', 1);
	// 		check_putchar_fd('a', 2);
	// 		check_putchar_fd(0, 2);
	// 		check_putchar_fd('a', 0);
	// 		check_putchar_fd(0, 0);
	// 	}
	// }
	// if (!t->p || !strcmp("ft_putstr_fd", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_putstr_fd\033[0m\n");
	// 	printf("\033[1;33mWARNING: Cannot validate this function.\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_d(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_putstr_fd(sunesc(argv[t->p + 2]), atoi(argv[t->p + 3]));
	// 	}
	// 	else if (t->v)
	// 	{
	// 		check_putstr_fd("GUYS WHAT HAPPENED HOLY SHIT", 1);
	// 		check_putstr_fd("oh nothing i guess", 2);
	// 		check_putstr_fd("nevermind", 0);
	// 		check_putstr_fd("", 1);
	// 		check_putstr_fd("", 2);
	// 		check_putstr_fd(0, 2);
	// 		check_putstr_fd("", 0);
	// 		check_putstr_fd(0, 0);
	// 	}
	// }
	// if (!t->p || !strcmp("ft_putendl_fd", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_putendl_fd\033[0m\n");
	// 	printf("\033[1;33mWARNING: Cannot validate this function.\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_d(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_putendl_fd(sunesc(argv[t->p + 2]), atoi(argv[t->p + 3]));
	// 	}
	// 	else if (t->v)
	// 	{
	// 		check_putendl_fd("GUYS WHAT HAPPENED HOLY SHIT", 1);
	// 		check_putendl_fd("oh nothing i guess", 2);
	// 		check_putendl_fd("nevermind", 0);
	// 		check_putendl_fd("", 1);
	// 		check_putendl_fd("", 2);
	// 		check_putendl_fd(0, 2);
	// 		check_putendl_fd("", 0);
	// 		check_putendl_fd(0, 0);
	// 	}
	// }
	// if (!t->p || !strcmp("ft_putnbr_fd", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_putnbr_fd\033[0m\n");
	// 	printf("\033[1;33mWARNING: Cannot validate this function.\n");
	// 	if (t->p && i_d(argv[t->p + 2]) && i_d(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_putnbr_fd(atoi(argv[t->p + 2]), atoi(argv[t->p + 3]));
	// 	}
	// 	else if (t->v)
	// 	{
	// 		check_putnbr_fd(4567, 0);
	// 		check_putnbr_fd(-1235235, 1);
	// 		check_putnbr_fd(2147483647, 0);
	// 		check_putnbr_fd(0, 1);
	// 		check_putnbr_fd(-2147483648, 2);
	// 		check_putnbr_fd(0, 2);
	// 		check_putnbr_fd(-2147483648, 0);
	// 		check_putnbr_fd(0, 0);
	// 	}
	// }
	// if (!t->p || !strcmp("ft_lstnew", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_lstnew\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]))
	// 	{
	// 		t->v = 1;
	// 		check_lstnew(sunesc(argv[t->p + 2]));
	// 	}
	// 	else
	// 	{
	// 		check_lstnew("allo");
	// 		check_lstnew(0);
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_lstadd_front", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_lstadd_front\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_lstadd_front(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]));
	// 	}
	// 	else
	// 	{
	// 		check_lstadd_front("bye", "allo");
	// 		check_lstadd_front(0, "allo");
	// 		check_lstadd_front("bye", 0);
	// 		check_lstadd_front(0, 0);
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_lstsize", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_lstsize\033[0m\n");
	// 	lst = malloc (5 * sizeof(t_list));
	// 	*lst = ft_lstnew("allo");
	// 	ft_lstadd_front(lst, ft_lstnew("salut"));
	// 	ft_lstadd_front(lst, ft_lstnew("bonjour"));
	// 	ft_lstadd_front(lst, ft_lstnew("au revoir"));
	// 	ft_lstadd_front(lst, ft_lstnew("monde"));
	// 	check_lstsize(*lst, 5);
	// 	free(lst);
	// 	check_lstsize(0, 0);
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_lstlast", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_lstlast\033[0m\n");
	// 	lst = malloc (5 * sizeof(t_list));
	// 	*lst = ft_lstnew("allo");
	// 	ft_lstadd_front(lst, ft_lstnew("salut"));
	// 	ft_lstadd_front(lst, ft_lstnew("bonjour"));
	// 	ft_lstadd_front(lst, ft_lstnew("au revoir"));
	// 	ft_lstadd_front(lst, ft_lstnew("monde"));
	// 	check_lstlast(*lst, "allo");
	// 	free(lst);
	// 	check_lstlast(0, 0);
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_lstadd_back", argv[t->p + 1]))
	// {
	// 	t->f = 0;
	// 	printf("\n\033[39;1;4mft_lstadd_back\033[0m\n");
	// 	if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]))
	// 	{
	// 		t->v = 1;
	// 		check_lstadd_back(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]));
	// 	}
	// 	else
	// 	{
	// 		check_lstadd_back("bye", "allo");
	// 		check_lstadd_back(0, "allo");
	// 		check_lstadd_back("bye", 0);
	// 		check_lstadd_back(0, 0);
	// 	}
	// 	if (!t->f)
	// 	{
	// 		printf("\033[1;32mSuccess.\n");
	// 		if (t->s == t->t)
	// 			t->s++;
	// 	}
	// 	else if (t->f == 1)
	// 		printf("\033[1;31mFailure.\n");
	// 	t->t++;
	// }
	// if (!t->p || !strcmp("ft_lstdelone", argv[t->p + 1]))
	// {
	// 	printf("\n\033[39;1;4mft_lstdelone\033[0m\n");
	// 	printf("\033[1;33mWARNING: Haven't written a test for this one yet.\n");
	// }
	// if (!t->p || !strcmp("ft_lstclear", argv[t->p + 1]))
	// {
	// 	printf("\n\033[39;1;4mft_lstclear\033[0m\n");
	// 	printf("\033[1;33mWARNING: Haven't written a test for this one yet.\n");
	// }
	// if (!t->p || !strcmp("ft_lstiter", argv[t->p + 1]))
	// {
	// 	printf("\n\033[39;1;4mft_lstiter\033[0m\n");
	// 	printf("\033[1;33mWARNING: Haven't written a test for this one yet.\n");
	// }
	// if (!t->p || !strcmp("ft_lstmap", argv[t->p + 1]))
	// {
	// 	printf("\n\033[39;1;4mft_lstmap\033[0m\n");
	// 	printf("\033[1;33mWARNING: Haven't written a test for this one yet.\n");
	// }
	if (!t.p)
	{
		if (t.s * 100 / t.t >= 50)
			printf("\033[1;32m");
		else
			printf("\033[1;31m");
		printf("\nScore: %d/%d (%d%%)\033[1;0m\n\n", t.s, t.t, t.s * 100 / t.t);
	}
	else
		printf("\033[1;0m\n");
}
