/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strlcat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:36 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_strlcat(char *dst, const char *src, size_t dstsize, t_test *t)
{
	size_t	r[2];
	char	*s[2];

	if (!dst || !src)
	{
		ft_strlcat(dst, src, dstsize);
		if (t->v)
			printf("\033[1;32mSuccess on dst = %s, src = %s, dstsize = %lu.\n", sesc(dst), sesc(src), dstsize);
	}
	else
	{
		s[0] = malloc((strlen(dst) + strlen(src) + 1) * sizeof(char));
		strlcpy(s[0], dst, strlen(dst) + strlen(src) + 1);
		r[0] = ft_strlcat(s[0], src, dstsize);
		s[1] = malloc((strlen(dst) + strlen(src) + 1) * sizeof(char));
		strlcpy(s[1], dst, strlen(dst) + strlen(src) + 1);
		r[1] = strlcat(s[1], src, dstsize);
		if (strcmp(s[0], s[1]) || r[0] != r[1])
		{
			printf("\033[1;31mFailure on dst = %s, src = %s, dstsize = %lu.\n", sesc(dst), sesc(src), dstsize);
			printf("ft_strlcat:	%s (%lu)\n", sesc(s[0]), r[0]);
			printf("strlcat:	%s (%lu)\n", sesc(s[1]), r[1]);
			t->f = 1;
		}
		else if (t->v)
		{
			printf("\033[1;32mSuccess on dst = %s, src = %s, dstsize = %lu.\n", sesc(dst), sesc(src), dstsize);
			printf("ft_strlcat:	%s (%lu)\n", sesc(s[0]), r[0]);
			printf("strlcat:	%s (%lu)\n", sesc(s[1]), r[1]);
		}
		free(*s);
	}
}

void	test_strlcat(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strlcat", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strlcat\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_strlcat(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_strlcat("hello world", "halla warld", 22, t);
			check_strlcat("bye wor", "baa warld", 22, t);
			check_strlcat("", "", 0, t);
			check_strlcat("ok", "no", 0, t);
			check_strlcat("ok", "", 6, t);
			check_strlcat("ok", "no", 6, t);
			check_strlcat("hahahahaha", "hohohohoho", 12, t);
			check_strlcat("hahahahaha", "hohohohoho", 0, t);
			check_strlcat("hahahahaha", "hohohohoho", 40, t);
			check_strlcat("rrrrrr", "lorem ipsum dolor sit amet", 6, t);
			check_strlcat(0, "lorem ipsum dolor sit amet", 6, t);
			check_strlcat("rrrrrr", 0, 6, t);
			check_strlcat("rrrrrr", "lorem ipsum dolor sit amet", 0, t);
			check_strlcat(0, 0, 6, t);
			check_strlcat(0, "lorem ipsum dolor sit amet", 0, t);
			check_strlcat("rrrrrr", 0, 0, t);
			check_strlcat(0, 0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_toupper(t, argv);
}
