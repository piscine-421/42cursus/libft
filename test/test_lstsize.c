/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_lstsize.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:44 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_lstsize(t_list *lst, int expected)
{
	if (ft_lstsize(lst) != expected)
	{
		printf("\033[1;31mFailure\n");
		printf("ft_lstsize:	%d\n", ft_lstsize(lst));
		printf("expected:	%d\n", expected);
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess\n");
		printf("ft_lstsize:	%d\n", ft_lstsize(lst));
		printf("expected:	%d\n", expected);
	}
}
