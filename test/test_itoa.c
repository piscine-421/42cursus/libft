/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_itoa.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:38 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_itoa(char *n)
{
	char	*n2;

	n2 = ft_itoa(atoi(n));
	if (strcmp(n2, n))
	{
		printf("\033[1;31mFailure on n = %d\n", atoi(n));
		printf("ft_itoa:	%s\n", sesc(n2));
		printf("expected:	%s\n", sesc(n));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on n = %d\n", atoi(n));
		printf("ft_itoa:	%s\n", sesc(n2));
		printf("expected:	%s\n", sesc(n));
	}
	free(n2);
}
