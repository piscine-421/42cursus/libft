/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strdup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:32 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_strdup(const char *s1)
{
	if (!s1)
	{
		ft_strdup(s1);
		if (g_verbose)
			printf("\033[1;32mSuccess on s1 = %s\n", sesc(s1));
	}
	else if (strcmp(strdup(s1), ft_strdup(s1)))
	{
		printf("\033[1;31mFailure on s1 = %s\n", sesc(s1));
		printf("ft_strdup:	%s\n", sesc(ft_strdup(s1)));
		printf("strdup:		%s\n", sesc(strdup(s1)));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on s1 = %s\n", sesc(s1));
		printf("ft_strdup:	%s\n", sesc(ft_strdup(s1)));
		printf("strdup:		%s\n", sesc(strdup(s1)));
	}
}
