/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_toupper.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:47 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_toupper(int c, t_test *t)
{
	if (ft_toupper(c) != toupper(c))
	{
		printf("\033[1;31mFailure on c = %s.\n", cesc(c));
		printf("ft_toupper:	%s\n", cesc(ft_toupper(c)));
		printf("toupper:	%s\n", cesc(toupper(c)));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on c = %s.\n", cesc(c));
		printf("ft_toupper:	%s\n", cesc(ft_toupper(c)));
		printf("toupper:	%s\n", cesc(toupper(c)));
	}
}

void	test_toupper(t_test *t, char **argv)
{
	int	i;

	if (!t->p || !strcmp("ft_toupper", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_toupper\033[0m\n");
		i = 0;
		if (t->p && i_c(argv[t->p + 2]))
		{
			t->v = 1;
			check_toupper(cunesc(argv[t->p + 2]), t);
		}
		else
			while (i < 256)
				check_toupper(i++, t);
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_tolower(t, argv);
}
