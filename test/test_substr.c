/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_substr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:45 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_substr(char const *s, unsigned int start, unsigned long len, char *expected)
{
	if (!strcmp(expected, "ARGMODE"))
	{
		printf("\033[1;33mWARNING: Cannot validate manually selected arguments.\n");
		printf("s = %s, start = %u, len = %lu\n", sesc(s), start, len);
		printf("ft_substr:	%s\n", sesc(ft_substr(s, start, len)));
		g_failure = 2;
	}
	else if (!strcmp(expected, "NULL"))
	{
		ft_substr(s, start, len);
		if (g_verbose)
			printf("\033[1;32mSuccess on s = %s, start = %u, len = %lu\n", sesc(s), start, len);
	}
	else if (strcmp(ft_substr(s, start, len), expected))
	{
		printf("\033[1;31mFailure on s = %s, start = %u, len = %lu\n", sesc(s), start, len);
		printf("ft_substr:	%s\n", sesc(ft_substr(s, start, len)));
		printf("expected:	%s\n", sesc(expected));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on s = %s, start = %u, len = %lu\n", sesc(s), start, len);
		printf("ft_substr:	%s\n", sesc(ft_substr(s, start, len)));
		printf("expected:	%s\n", sesc(expected));
	}
}
