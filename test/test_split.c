/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_split.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:29 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_split(char const *s, char c, char **expected)
{
	int		f;
	int		i;
	char	**split;

	f = 0;
	i = 0;
	if (!strcmp(expected[0], "ARGMODE"))
	{
		split = ft_split(s, c);
		printf("\033[1;33mWARNING: Cannot validate manually selected arguments.\n");
		printf("s = %s, c = %s\n", sesc(s), cesc(c));
		while (split[i])
		{
			printf("ft_split[%d]:	%s\n", i, sesc(split[i]));
			i++;
		}
		g_failure = 2;
	}
	else if (!strcmp(expected[0], "NULL"))
	{
		ft_split(s, c);
		if (g_verbose)
			printf("\033[1;32mSuccess on s = %s, c = %s\n", sesc(s), cesc(c));
	}
	else
	{
		split = ft_split(s, c);
		while (expected[i] || split[i])
		{
			if ((!expected[i] || !split[i]) || strcmp(split[i], expected[i]))
				f = 1;
			i++;
		}
		i = 0;
		if (f == 1)
		{
			printf("\033[1;31mFailure on s = %s, c = %s\n", sesc(s), cesc(c));
			while (expected[i])
			{
				printf("ft_split[%d]:	%s\n", i, sesc(split[i]));
				printf("expected[%d]:	%s\n", i, sesc(expected[i]));
				i++;
			}
			g_failure = 1;
		}
		else if (g_verbose && !g_failure)
		{
			printf("\033[1;32mSuccess on s = %s, c = %s\n", sesc(s), cesc(c));
			while (split[i])
			{
				printf("ft_split[%d]:	%s\n", i, sesc(split[i]));
				printf("expected[%d]:	%s\n", i, sesc(expected[i]));
				i++;
			}
		}
	}
}
