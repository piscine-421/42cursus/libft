/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_memmove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:47 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_memmove(void *dst, const void *src, size_t len, t_test *t)
{
	char	*s[4];

	if (!dst || !src)
	{
		ft_memmove(dst, src, len);
		if (t->v)
			printf("\033[1;32mSuccess on dst = %s, src = %s, n = %lu.\n", sesc(dst), sesc(src), len);
	}
	else
	{
		s[0] = strdup(dst);
		ft_memmove(s[0], src, len);
		s[1] = strdup(dst);
		memmove(s[1], src, len);
		if (strcmp(s[0], s[1]))
		{
			printf("\033[1;31mFailure on dst = %s, src = %s, n = %lu.\n", sesc(dst), sesc(src), len);
			printf("ft_memmove:	%s\n", sesc(s[0]));
			printf("memmove:	%s\n", sesc(s[1]));
			t->f = 1;
		}
		else if (t->v)
		{
			printf("\033[1;32mSuccess on dst = %s, src = %s, n = %lu.\n", sesc(dst), sesc(src), len);
			printf("ft_memmove:	%s\n", sesc(s[0]));
			printf("memmove:	%s\n", sesc(s[1]));
		}
		free(*s);
	}
}

void	test_memmove(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_memmove", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_memmove\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_s(argv[t->p + 3]) && i_d(argv[t->p + 4]))
		{
			t->v = 1;
			check_memmove(sunesc(argv[t->p + 2]), sunesc(argv[t->p + 3]), atoi(argv[t->p + 4]), t);
		}
		else
		{
			check_memmove("hello world", "halla warld", 11, t);
			check_memmove("bye wor", "baa warld", 11, t);
			check_memmove("", "", 0, t);
			check_memmove("ok", "no", 0, t);
			check_memmove("ok", "", 3, t);
			check_memmove("ok", "no", 3, t);
			check_memmove("afhafdh", "sdgdagaw", 32, t);
			check_memmove("orem ipsum dolor sit a", "consectetur", 5, t);
			check_memmove(0, 0, 0, t);
			check_memmove(0, 0, 5, t);
			check_memmove(0, "consectetur", 0, t);
			check_memmove("orem ipsum dolor sit a", 0, 0, t);
			check_memmove("orem ipsum dolor sit a", "consectetur", 0, t);
			check_memmove("orem ipsum dolor sit a", 0, 5, t);
			check_memmove(0, "consectetur", 5, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_strlcpy(t, argv);
}
