/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_isalpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:34 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_isalpha(int c, t_test *t)
{
	if ((ft_isalpha(c) && !isalpha(c)) || (!ft_isalpha(c) && isalpha(c)))
	{
		printf("\033[1;31mFailure on c = %s.\n", cesc(c));
		printf("ft_isalpha:	%d\n", ft_isalpha(c));
		printf("isalpha:	%d\n", isalpha(c));
		g_failure = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on c = %s.\n", cesc(c));
		printf("ft_isalpha:	%d\n", ft_isalpha(c));
		printf("isalpha:	%d\n", isalpha(c));
	}
}

void	test_isalpha(t_test *t, char **argv)
{
	int	i;

	if (!t->p || !strcmp("ft_isalpha", argv[t->p + 1]))
	{
		i = -1;
		t->f = 0;
		printf("\n\033[39;1;4mft_isalpha\033[0m\n");
		if (t->p && i_c(argv[t->p + 2]))
		{
			t->v = 1;
			check_isalpha(cunesc(argv[t->p + 2]), t);
		}
		else
			while (i < 256)
				check_isalpha(i++, t);
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_isdigit(t, argv);
}
