/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_strchr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:31 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void	check_strchr(const char *s, int c, t_test *t)
{
	if (!s)
	{
		ft_strchr(s, c);
		if (t->v)
			printf("\033[1;32mSuccess on s = %s, c = %s\n", sesc(s), cesc(c));
	}
	else if (strchr(s, c) != ft_strchr(s, c))
	{
		printf("\033[1;31mFailure on s = %s, c = %s\n", sesc(s), cesc(c));
		printf("ft_strchr:	%s\n", sesc(ft_strchr(s, c)));
		printf("strchr:		%s\n", sesc(strchr(s, c)));
		t->f = 1;
	}
	else if (t->v)
	{
		printf("\033[1;32mSuccess on s = %s, c = %s\n", sesc(s), cesc(c));
		printf("ft_strchr:	%s\n", sesc(ft_strchr(s, c)));
		printf("strchr:		%s\n", sesc(strchr(s, c)));
	}
}

void	test_strchr(t_test *t, char **argv)
{
	if (!t->p || !strcmp("ft_strchr", argv[t->p + 1]))
	{
		t->f = 0;
		printf("\n\033[39;1;4mft_strchr\033[0m\n");
		if (t->p && i_s(argv[t->p + 2]) && i_c(argv[t->p + 3]))
		{
			t->v = 1;
			check_strchr(sunesc(argv[t->p + 2]), cunesc(argv[t->p + 3]), t);
		}
		else
		{
			check_strchr("ALLO TOUT LE MONDE", 'T', t);
			check_strchr("SDGSGRWRDG", 'a', t);
			check_strchr("", 'a', t);
			check_strchr("SRGSQW", '\0', t);
			check_strchr("", '\0', t);
			check_strchr("asdgaryertje75srhdrwerq3vtws", 't' + 256, t);
			check_strchr(0, 't' + 256, t);
			check_strchr("asdgaryertje75srhdrwerq3vtws", 0, t);
			check_strchr(0, 0, t);
		}
		if (!t->f)
		{
			printf("\033[1;32mSuccess.\n");
			if (t->s == t->t)
				t->s++;
		}
		else
			printf("\033[1;31mFailure.\n");
		t->t++;
	}
	test_strrchr(t, argv);
}
