/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:26:51 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_H
# define TEST_H
# include <ctype.h>
# include "../Libft/libft.h"
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

int	g_failure;
int	g_verbose;

typedef struct s_test
{
	int	f;
	int	p;
	int	s;
	int	t;
	int	v;
} t_test;

int		i_c(char *s);
int		i_d(char *s);
int		i_s(char *s);
char	*cesc(const char c);
char	cunesc(const char *str);
char	*sesc(const char *str);
char	*sunesc(const char *str);
void	test_isalpha(t_test *t, char **argv);
void	test_isdigit(t_test *t, char **argv);
void	test_isalnum(t_test *t, char **argv);
void	test_isascii(t_test *t, char **argv);
void	test_isprint(t_test *t, char **argv);
void	test_strlen(t_test *t, char **argv);
void	test_memset(t_test *t, char **argv);
void	test_bzero(t_test *t, char **argv);
void	test_memcpy(t_test *t, char **argv);
void	test_memmove(t_test *t, char **argv);
void	test_strlcpy(t_test *t, char **argv);
void	test_strlcat(t_test *t, char **argv);
void	test_toupper(t_test *t, char **argv);
void	test_tolower(t_test *t, char **argv);
void	test_strchr(t_test *t, char **argv);
void	test_strrchr(t_test *t, char **argv);
void	test_strncmp(t_test *t, char **argv);
void	test_memchr(t_test *t, char **argv);
void	test_memcmp(t_test *t, char **argv);
void	test_strnstr(t_test *t, char **argv);
void	test_atoi(t_test *t, char **argv);
void	test_calloc(t_test *t, char **argv);
void	test_strdup(const char *s1);
void	test_substr(char const *s, unsigned int start, unsigned long len, char *expected);
void	test_strjoin(char const *s1, char const *s2, char *expected);
void	test_strtrim(char const *s1, char const *set, char *expected);
void	test_split(char const *s, char c, char **expected);
void	test_itoa(char *n);
char	changec(unsigned int n, char c);
char	shakyc(unsigned int n, char c);
void	test_strmapi(char const *s, char (*f)(unsigned int, char), char *expected, char *fname);
void	changes(unsigned int n, char *s);
void	shakys(unsigned int n, char *s);
void	test_striteri(char *s, void (*f)(unsigned int, char*), char *expected, char *fname);
void	test_putchar_fd(char c, int fd);
void	test_putstr_fd(char *s, int fd);
void	test_putendl_fd(char *s, int fd);
void	test_putnbr_fd(int n, int fd);
void	test_lstnew(void *content);
void	test_lstadd_front(char *lstc, char *newc);
void	test_lstsize(t_list *lst, int expected);
void	test_lstlast(t_list *lst, char *expected);
void	test_lstadd_back(char *lstc, char *newc);

#endif
