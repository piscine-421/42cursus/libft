/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_lstadd_back.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:39 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

void	test_lstadd_back(char *lstc, char *newc)
{
	t_list	**lst;
	t_list	*new;

	lst = malloc(sizeof(t_list) * 2);
	*lst = ft_lstnew(lstc);
	new = ft_lstnew(newc);
	ft_lstadd_back(lst, new);
	if (lstc && newc && (strcmp(ft_lstlast(*lst)->content, new->content)))
	{
		printf("\033[1;31mFailure on *lst->content = %s, new->content = %s\n", sesc(lstc), sesc(newc));
		printf("ft_lstlast(ft_lstadd_back[0])->content:		%s\n", sesc(ft_lstlast(*lst)->content));
		printf("new->content:					%s\n", sesc(new->content));
		g_failure = 1;
	}
	else if (g_verbose)
	{
		printf("\033[1;32mSuccess on *lst->content = %s, new->content = %s\n", sesc(lstc), sesc(newc));
		printf("ft_lstlast(ft_lstadd_back[0])->content:		%s\n", sesc(ft_lstlast(lst[0])->content));
		printf("new->content:					%s\n", sesc(new->content));
	}
	free(lst);
	free(new);
}
