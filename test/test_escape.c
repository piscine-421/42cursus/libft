/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_escape.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/13 08:23:32 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

char	*cesc(const char c)
{
	int		i;
	int		n;
	char	*str;

	i = 0;
	n = 1;
	if (c == '\a' || c == '\b' || c == '\n' || c == '\v' || c == '\f' || c == '\r' || c == '\e')
		n++;
	else if (!isprint(c))
	{
		n++;
		if ((unsigned char)c > 9)
			n++;
		if ((unsigned char)c > 99)
			n++;
	}
	str = malloc((n + 3) * sizeof(char));
	str[i++] = '\'';
	if (c == '\a')
	{
		str[i++] = '\\';
		str[i++] = 'a';
	}
	else if (c == '\b')
	{
		str[i++] = '\\';
		str[i++] = 'b';
	}
	else if (c == '\n')
	{
		str[i++] = '\\';
		str[i++] = 'n';
	}
	else if (c == '\v')
	{
		str[i++] = '\\';
		str[i++] = 'v';
	}
	else if (c == '\f')
	{
		str[i++] = '\\';
		str[i++] = 'f';
	}
	else if (c == '\r')
	{
		str[i++] = '\\';
		str[i++] = 'r';
	}
	else if (c == '\e')
	{
		str[i++] = '\\';
		str[i++] = 'e';
	}
	else if (!isprint(c) && c != '\t')
	{
		str[i++] = '\\';
		str[i++] = ft_itoa((unsigned char)c)[0];
		if ((unsigned char)c > 9)
			str[i++] = ft_itoa((unsigned char)c)[1];
		if ((unsigned char)c > 99)
			str[i++] = ft_itoa((unsigned char)c)[2];
	}
	else
		str[i++] = c;
	str[i++] = '\'';
	str[i++] = '\0';
	return (str);
}

char	cunesc(const char *str)
{
	char	c;

	if (str[0] == '\\')
	{
		if (str[1] == 'a')
			c = '\a';
		else if (str[1] == 'b')
			c = '\b';
		else if (str[1] == 't')
			c = '\t';
		else if (str[1] == 'n')
			c = '\n';
		else if (str[1] == 'v')
			c = '\v';
		else if (str[1] == 'f')
			c = '\f';
		else if (str[1] == 'r')
			c = '\r';
		else if (str[1] == 'e')
			c = '\e';
		else if (str[1] == '\\')
			c = '\\';
		else if (str[1] == '\"')
			c = '\"';
		else if (str[1] == '\'')
			c = '\'';
		else
			c = atoi(&str[1]);
	}
	else
		c = str[0];
	return (c);
}

char	*sesc(const char *str)
{
	int		i;
	int		i2;
	int		n;
	char	*str2;

	i = 0;
	i2 = 0;
	n = 0;
	if (!str)
		return ("NULL");
	if (str[0] == '\0')
		return ("\"\\0\"");
	while (str[i])
	{
		if (str[i] == '\a' || str[i] == '\b' || str[i] == '\n' || str[i] == '\v' || str[i] == '\f' || str[i] == '\r' || str[i] == '\e')
			n++;
		else if (!isprint(str[i]))
		{
			n++;
			if ((unsigned char)str[i] > 9)
				n++;
			if ((unsigned char)str[i] > 99)
				n++;
		}
		i++;
	}
	str2 = malloc((strlen(str) + n + 3) * sizeof(char));
	i = 0;
	str2[i2++] = '\"';
	while (str[i])
	{
		if (str[i] == '\a')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'a';
		}
		else if (str[i] == '\b')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'b';
		}
		else if (str[i] == '\n')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'n';
		}
		else if (str[i] == '\v')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'v';
		}
		else if (str[i] == '\f')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'f';
		}
		else if (str[i] == '\r')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'r';
		}
		else if (str[i] == '\e')
		{
			str2[i2++] = '\\';
			str2[i2++] = 'e';
		}
		else if (!isprint(str[i]) && str[i] != '\t')
		{
			str2[i2++] = '\\';
			str2[i2++] = ft_itoa((unsigned char)str[i])[0];
			if ((unsigned char)str[i] > 9)
				str2[i2++] = ft_itoa((unsigned char)str[i])[1];
			if ((unsigned char)str[i] > 99)
				str2[i2++] = ft_itoa((unsigned char)str[i])[2];
		}
		else
			str2[i2++] = str[i];
		i++;
	}
	str2[i2++] = '\"';
	str2[i2++] = '\0';
	return (str2);
}

char	*sunesc(const char *str)
{
	int		i;
	int		i2;
	char	*str2;

	i = 0;
	i2 = 0;
	if (!strcmp("NULL", str))
		return (0);
	str2 = malloc(strlen(str));
	while (str[i])
	{
		if (str[i] == '\\')
		{
			i++;
			if (str[i] == 'a')
				str2[i2] = '\a';
			else if (str[i] == 'b')
				str2[i2] = '\b';
			else if (str[i] == 't')
				str2[i2] = '\t';
			else if (str[i] == 'n')
				str2[i2] = '\n';
			else if (str[i] == 'v')
				str2[i2] = '\v';
			else if (str[i] == 'f')
				str2[i2] = '\f';
			else if (str[i] == 'r')
				str2[i2] = '\r';
			else if (str[i] == 'e')
				str2[i2] = '\e';
			else if (str[i] == '\\')
				str2[i2] = '\\';
			else if (str[i] == '\"')
				str2[i2] = '\"';
			else if (str[i] == '\'')
				str2[i2] = '\'';
			else if (isdigit(str[i]))
			{
				str2[i2] = atoi(&str[i]);
				while (isdigit(str[i]))
					i++;
				i--;
			}
		}
		else
			str2[i2] = str[i];
		i++;
		i2++;
	}
	return (str2);
}
